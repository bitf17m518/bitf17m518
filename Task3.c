#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h> 
#include <math.h>
void * f1(void *);
struct mystruct{
   double number;
   double power; 
};
int main(int argc, char* argv[])
{
	if(argc>=2)
	{
		pthread_t tid1;
		struct mystruct st;
		st.number=atoi(argv[1]);
		st.power=atoi(argv[2]);
		pthread_create(&tid1, NULL, f1, (void*)&st);
		pthread_join(tid1, NULL);
		printf("\nChild thread teminated.....\nEntering infinite loop of parent thread\n");
		while(1);
	}
	else
		 printf("\nTwo arguments not given\n");
	return 0;
}
void * f1(void * args){
	struct mystruct p = *(struct mystruct*)args;
    float res=pow(p.number,p.power);
	printf( "%s%f", "Number: ",p.number );
	printf( "\n%s%f", "Power: ",p.power );
	printf( "\n%s", "Result: ");
	printf( "%f", res);
	
	pthread_exit(NULL);
}
