#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h> 
#include <math.h>
#include<string.h>
int main(int argc, char* argv[])
{
	if(argc!=2)
	{
	int size=0;
	char* filename=malloc(sizeof(argv[1]));
	int timeslice;
	filename=argv[1];
	timeslice=(int)*argv[2];
	timeslice-=48;
	FILE* fp;
	fp=fopen(filename,"r");
	if(fp==NULL)
	{
		
		printf("File not found");
		return 1;
	}
	else
	{
		
		int arrT;
		int burst;
		fscanf(fp, "%d", &arrT);
		int T=arrT;
		size=0;
		while(arrT!=-1)
		{
			fscanf(fp, "%d", &burst);
			size++;
			fscanf(fp, "%d", &arrT);
			
		}
		fclose(fp);
		int* bT=malloc(size * sizeof(int));
		int* aT=malloc(size * sizeof(int));
		int* wT=malloc(size * sizeof(int));
		int* rT=malloc(size * sizeof(int));
		int* fT=malloc(size * sizeof(int));
		int* TT=malloc(size * sizeof(int));
		fflush(fp);
		FILE* fs;
		
	    fs=fopen(filename,"r");
		if(fs!=NULL)
		{
			fscanf(fs, "%d", &arrT);
			for(int i=0;i<size;i++)
			{
				fscanf(fs, "%d", &burst);
				aT[i]=arrT;
				bT[i]=burst;
				rT[i]=burst;
				fT[i]=0;
				fscanf(fs, "%d", &arrT);
			
			}
			for(int i=0;i<size;i++)
			{
				for(int j=i;j<size;j++)
				{
					if(aT[i]>aT[j])
					{

						int max;
						max=bT[j];
						bT[j]=bT[i];
						bT[i]=max;
						max=rT[j];
						rT[j]=rT[i];
						rT[i]=max;
						max=aT[j];
						aT[j]=aT[i];
						aT[i]=max;
					}
				}
			}
			fclose(fp);
			int iter=0;
			int time=0;
			int countZeroes=0;
			while(countZeroes!=size)
			{
				int flag=0;
				for(int i=0;i<size;i++)
				{
					if(time>=aT[i] && rT[i]!=0)
					{
						break;
					}
					else if(i==size-1)
					{
						time++;
					}
					
				}
				for(int i=0;i<size;i++)
				{
					if(aT[i]<=time && rT[i]!=0)
					{
						if(timeslice>rT[i])
						{
							time+=rT[i];
							fT[i]=time;
							rT[i]=0;
							countZeroes++;
						}
                        else if(timeslice==rT[i])
						{
							time+=rT[i];
							fT[i]=time;
							countZeroes++;
							rT[i]=0;
						}
						else if(timeslice<rT[i])
						{
							
							time+=timeslice;
							rT[i]-=timeslice;
							if(rT[i]==0)
							{
								fT[i]=time;
							}
							
						}
						
					}
				}
			}
			float avgTTime=0;
			for(int i=0;i<size;i++)
			{
				wT[i]=fT[i]-bT[i]-aT[i];
				TT[i]=fT[i]-aT[i];
				avgTTime+=TT[i];
			}
		
			printf("\nProcess  ArrivalTime  BurstTime FinishTime WaitingTime TurnAroundTime\n");
			for(int i=0;i<size;i++)
				printf("P%d\t\t\%d\t %d \t  %d \t\t%d\t%d\n",i,aT[i],bT[i],fT[i],wT[i],TT[i]);
			printf("\n\n Average Turn around time: %f\n",avgTTime/size);
		return 0;
		
	}
	
	
}
	}
	else 
	{
		printf("\n\n Kindly pass filename & time quantum as argument/n");
		return 0;		
	}
}
